﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace navigation
{
   
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void NextPage1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }

        async void NextPage2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }

        async void NextPage3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }
    }
}
